<?php

declare(strict_types=1);
/**
 * This file is part of Hyperf.
 *
 * @link     https://www.hyperf.io
 * @document https://hyperf.wiki
 * @contact  group@hyperf.io
 * @license  https://github.com/hyperf/hyperf/blob/master/LICENSE
 */
return [
	\App\Listener\SocketIoConnectListener::class,
	\App\Listener\SocketIoDisConnectListener::class,
	\App\Listener\UserChatListener::class,
    \App\Listener\SendWelcomeMsgListener::class,
];
