<?php
namespace App\Listener;

use App\Event\SocketIoConnect;
use Hyperf\Event\Contract\ListenerInterface;
use Hyperf\Di\Annotation\Inject;
use Psr\Container\ContainerInterface;
use Hyperf\Redis\RedisFactory;

class SocketIoConnectListener implements ListenerInterface
{
    /**
     * @Inject
     * @var ContainerInterface
     */
    protected $container;

    public function listen(): array
    {
        // 返回一个该监听器要监听的事件数组，可以同时监听多个事件
        return [
            SocketIoConnect::class,
        ];
    }

    public function process(object $event)
    {
        // 事件触发后该监听器要执行的代码写在这里，比如该示例下的发送用户注册成功短信等
        // 直接访问 $event 的 user 属性获得事件触发时传递的参数值
        $user = $event->user;
        $sid = $event->sid;
        $redis = $this->container->get(RedisFactory::class)->get('chat');
        $redis->set((string)$user->id, (string)$sid);
        //这里可以设置用户在线
    }
}
