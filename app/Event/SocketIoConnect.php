<?php
namespace App\Event;

class SocketIoConnect
{
    // 建议这里定义成 public 属性，以便监听器对该属性的直接使用，或者你提供该属性的 Getter
    public $user;
    public $sid;
    
    public function __construct($user, $sid)
    {
        $this->user = $user;
        $this->sid = $sid;
    }
}
